\select@language {english}
\contentsline {section}{List of Figures}{I}
\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}General HJM-Framework and Set-Up}{3}
\contentsline {subsection}{\numberline {2.1}The Multiple Curve Framework: Some Notation}{3}
\contentsline {subsubsection}{\numberline {2.1.1}The discount curve}{4}
\contentsline {subsubsection}{\numberline {2.1.2}Risk-Neutral and Forward Measure}{4}
\contentsline {subsubsection}{\numberline {2.1.3}The LIBOR rate}{5}
\contentsline {subsubsection}{\numberline {2.1.4}The OIS-LIBOR basis spread}{7}
\contentsline {subsection}{\numberline {2.2}Arbitrage-Free Modeling of Spread and OIS Rate}{8}
\contentsline {subsubsection}{\numberline {2.2.1}Arbitrage-Free OIS rate - First drift condition}{9}
\contentsline {subsubsection}{\numberline {2.2.2}Arbitrage-Free spread rate - Second drift condition}{11}
\contentsline {section}{\numberline {3}A Trolle-Schwartz Stochastic Spread Model and its Simulation}{15}
\contentsline {subsection}{\numberline {3.1}The specific model set-up inspired by Trolle-Schwartz}{15}
\contentsline {subsubsection}{\numberline {3.1.1}The Forward Rate Dynamics of Spread and OIS}{15}
\contentsline {subsubsection}{\numberline {3.1.2}Stochastic Volatility Processes and Correlation}{16}
\contentsline {subsection}{\numberline {3.2}State Variables and Coefficients for Simulating the Zero Coupon Bonds}{18}
\contentsline {subsubsection}{\numberline {3.2.1}Explicit Deterministic Volatility Factors}{18}
\contentsline {subsubsection}{\numberline {3.2.2}OIS Zero Coupon Bond Dynamics}{18}
\contentsline {subsubsection}{\numberline {3.2.3}Spread Zero Coupon Bond Dynamics}{22}
\contentsline {section}{\numberline {4}Semi-Analytical Option Pricing Within the Stochastic Spread Model}{31}
\contentsline {subsection}{\numberline {4.1}European Put and Call Options}{31}
\contentsline {subsubsection}{\numberline {4.1.1}Closed Formula without Approximation}{31}
\contentsline {subsubsection}{\numberline {4.1.2}Approximation Approaches}{38}
\contentsline {subsection}{\numberline {4.2}Caps and Floors}{39}
\contentsline {subsection}{\numberline {4.3}Swaptions}{41}
\contentsline {section}{\numberline {5}Verification and Conclusion}{43}
\contentsline {section}{\numberline {6}Appendix A}{45}
\contentsline {section}{References}{49}
