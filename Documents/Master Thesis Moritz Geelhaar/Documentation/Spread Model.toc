\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}General Model Set-Up}{2}
\contentsline {subsection}{\numberline {2.1}The Multiple Curve Framework: Our Notation}{2}
\contentsline {subsubsection}{\numberline {2.1.1}The discount curve model}{3}
\contentsline {subsubsection}{\numberline {2.1.2}Risk-Neutral and Forward Measure}{3}
\contentsline {subsubsection}{\numberline {2.1.3}The spread rate (e.g.\ LIBOR rate)}{4}
\contentsline {subsubsection}{\numberline {2.1.4}The OIS-LIBOR basis spread}{5}
\contentsline {subsection}{\numberline {2.2}Arbitrage-free joint modeling of spread and OIS rate}{5}
\contentsline {subsubsection}{\numberline {2.2.1}Arbitrage-Free OIS rate - First drift condition}{6}
\contentsline {subsubsection}{\numberline {2.2.2}Arbitrage-Free spread rate - Second drift condition}{8}
\contentsline {section}{\numberline {3}A Trolle-Schwartz-type stochastic spread model with simulation scheme}{11}
\contentsline {subsection}{\numberline {3.1}The specific model set-up employing the Trolle--Schwartz model}{11}
\contentsline {subsubsection}{\numberline {3.1.1}The Forward Rate Dynamics of Spread and OIS}{11}
\contentsline {subsubsection}{\numberline {3.1.2}Stochastic Volatility Processes and Correlation}{12}
\contentsline {subsection}{\numberline {3.2}State Variables and Coefficients for simulating the dynamics of zero coupon bonds}{12}
\contentsline {subsubsection}{\numberline {3.2.1}Explicit Deterministic Volatility Factors}{12}
\contentsline {subsubsection}{\numberline {3.2.2}Simulation scheme for the stochastic spread model}{13}
\contentsline {section}{\numberline {4}Semi-Analytical Option Pricing of European Put and Call Options}{23}
\contentsline {subsection}{\numberline {4.1}Fourier Transformation}{23}
\contentsline {subsection}{\numberline {4.2}Closed Formula for Put and Call Option}{29}
\contentsline {section}{\numberline {5}Verification and Conclusion}{30}
\contentsline {section}{References}{33}
