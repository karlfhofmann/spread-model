\documentclass{beamer}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{beamerthemeshadow}
\usetheme{CambridgeUS}
\beamersetuncovermixins{\opaqueness<1>{25}}{\opaqueness<2->{15}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Prop}{\mathbb{P}}
\newcommand{\sa}{\mathcal{F}}
\newcommand{\PQ}{\overline{B}_\delta} %Libor Bond
\newcommand{\PA}{\tilde{B}_\delta} %Spread Bond
\newcommand{\PD}{B} %OIS Bond
\newcommand{\AD}{\tilde{A}_\delta} %drift integral spread
\newcommand{\SD}{\tilde{\Sigma}_\delta} %vola integral spread
\newcommand{\B}{B^\ast} %money market account
\newcommand{\fg}{f}
\newcommand{\yg}{y^\delta}
\newcommand{\yx}{y^\delta}
\newcommand{\mux}{\mu_y^\delta}
\newcommand{\sigmax}{\sigma_y^\delta}
\newcommand{\vx}{v_y^\delta}
\newcommand{\kappax}{\kappa_y^\delta}
\newcommand{\thetax}{\theta_y^\delta}
\newcommand{\sigmavx}{\sigma_{v_y^\delta}}
\newcommand{\gammax}{\gamma_y^\delta}
\newcommand{\alphax}{\alpha_y^\delta}
\definecolor{deloittegreen}{rgb}{0.525, 0.737, 0.145} 

%\setbeamercolor*{parlette primary}{use=structure,fg=red,bg=blue!50!black}
\setbeamercolor*{palette secondary}{use=structure,fg=deloittegreen,bg=white} %Titel unten
\setbeamercolor*{palette tertiary}{use=structure,fg=white,bg=deloittegreen} %Autor unten
\setbeamercolor{frametitle}{fg=deloittegreen, bg=black}
\setbeamercolor{date in head/foot}{fg=deloittegreen, bg=black}
%\setbeamercolor*{date in head/foot}{parent=palette primary}
%\setbeamercolor*{subsection in head/foot}{parent=palette primary}

\setbeamercolor{block title}{use=structure,fg=white,bg=deloittegreen}
\setbeamercolor{block body}{use=structure,fg=black,bg=white!20!white}
\setbeamercolor{title}{use=structure,fg=white,bg=deloittegreen}
\setbeamercolor{item}{fg=deloittegreen}
\setbeamercovered{invisible}

%\setbeamertemplate{section in toc}[ball]
\setbeamertemplate{section in toc}{%
  {\color{deloittegreen}\inserttocsectionnumber.}~\inserttocsection}

\AtBeginSection[]
{
   \begin{frame}
        \frametitle{Table of Contents}
        \tableofcontents[currentsection,currentsubsection]
   \end{frame}
}

\AtBeginSubsection[]
{
   \begin{frame}
        \frametitle{Table of Contents}
        \tableofcontents[currentsection,currentsubsection]
   \end{frame}
}

\logo{\includegraphics{deloitte.jpg}}

\begin{document}
\title{A Stochastic Spread Model}  
\author{Deloitte.}
\date{\today} 


\frame{\titlepage}

\section{The Model}

\frame{\frametitle{General Set Up}
\begin{block}{OIS and LIBOR Bonds}
Let $B(t,T)$ denote the OIS zero coupon bond with maturity $T$ and let $f(t,T)$ denote the according time-$t$ instantaneous forward OIS rate, i.e.:
\begin{equation*}
B(t,T):=\exp\bigg(-\int_t^T{f(t,u)du}\bigg)
\end{equation*}
Furthermore, the LIBOR bonds are fictitious bonds that are connected to the LIBOR rate $L(t,T,T+\delta)$:
\begin{equation*}
\begin{split}
L(t,T,T+\delta)=&\frac{1}{\delta}\E^{T+\delta}\bigg[\bigg(\frac{1}{\PQ(T,T+\delta)}-1\bigg)\bigg|\mathcal{F}_t\bigg]\\
=:&\frac{1}{\delta}\bigg(\frac{\PQ(t,T)}{\PQ(t,T+\delta)}-1\bigg)
\end{split}
\end{equation*}
\end{block}
}

\frame{\frametitle{General Set Up}
\begin{block}{The spread}
Let $l^\delta(t,T)$ denote the according time-$t$ instantaneous forward OIS rate, i.e.:
\begin{equation*}
\PQ(t,T)=\exp\bigg(-\int_t^T{l^\delta(t,u)du}\bigg)
\end{equation*}
We introduce the basis spread on forward rate level:
\begin{equation*}
y^\delta(t,T)=l^\delta(t,T)-f(t,T)
\end{equation*}
The respective zero coupon bond is defined as usual:
\begin{equation*}
\PA(t,T):=\exp(-\int_t^T{y^\delta(t,u)du})
\end{equation*}
\end{block}
}

\frame{\frametitle{General Set Up (1)}
\begin{block}{OIS and Split Dynamics}
The model chosen for the OIS rate is the Trolle-Schwartz interest rate model for $N=1$. The OIS rate is given through
\begin{equation*}
\begin{split}
df(t,T)&=\mu_f(t,T)dt+\sigma_f(t,T)\sqrt{v_f(t)}dW_f(t)\\
dv_f(t)&=\kappa_f(\theta_f-v_f(t))dt+\sigma_{v_f}\sqrt{v_f(t)}dW_{v_f}(t)
\end{split}
\end{equation*}
The spread follows the dynamics of the Trolle-Schwartz commodity model:
\begin{equation*}
\begin{split}
dy^\delta(t,T)&=\mu^\delta_y(t,T)dt+\sigma_y^\delta(t,T)\sqrt{v^\delta_y(t)}dW_y(t)\\
dv^\delta_y(t)&=\kappa^\delta_y(\theta^\delta_y-v^\delta_y(t))dt+\sigma^\delta_{v_y}\sqrt{v^\delta_y(t)}dW_{v_y}(t)
\end{split}
\end{equation*}
$W_i$ here denote standard Wiener processes under risk-neutral measure $\Q$
\end{block}
}

\frame{\frametitle{General Set Up (2)}
\begin{block}{Correlation Matrix}
We furthermore assume the following correlation matrix for $W_{y^\delta}(t),W_{v_y^\delta}(t),W_f(t),W_{v_f}(t)$
\begin{equation*}
\begin{bmatrix}
  & \parbox[b][0pt][b]{.5em}{\raisebox{3ex}{$W_{\yx}$}}   & \parbox[b][0pt][b]{.5em}{\raisebox{3ex}{$W_{\vx}$}}   & \parbox[b][0pt][b]{.5em}{\raisebox{3ex}{$W_f$}}   & \parbox[b][0pt][b]{.5em}{\raisebox{3ex}{$W_{v_f}$}}   \\[-3ex]
\makebox[0pt][r]{$W_{\yx}$\hspace{1.5em}} & 1 & \rho_{\yx,\vx} & \rho_{\yx,f} & 0 \\
\makebox[0pt][r]{$W_{\vx}$\hspace{1.5em}} & \rho_{\yx,\vx} & 1 & 0 & 0 \\
\makebox[0pt][r]{$W_f$\hspace{1.5em}} & \rho_{\yx,f} & 0 & 1 & \rho_{f,v_f} \\
\makebox[0pt][r]{$W_{v_f}$\hspace{1.5em}} & 0 & 0 & \rho_{f,v_f} & 1 \\
\end{bmatrix}
\end{equation*}
While $\rho_{v_f,y^\delta}=\rho_{v_y^\delta,f}=0$ is very reasonable, one may question the initial assumption of $\rho_{v_f,v_y}=0$
\end{block}
}

\frame{\frametitle{Some Further Notation}
For simplicity we introduce the notation
\begin{equation*}
\begin{split}
A(t,T)&:=\int_t^T{\mu_f(t,u)du}\\
\Sigma(t,T)&:=\sqrt{v_f(t)}\int_t^T{\sigma_f(t,u)du}
\end{split}
\end{equation*}
and analogously for the spread:
\begin{equation*}
\begin{split}
\AD(t,T)&:=\int_t^T{\mu_y^\delta(t,u)du}\\
\SD(t,T)&:=\sqrt{v_y^\delta(t)}\int_t^T{\sigma_y^\delta(t,u)du}
\end{split}
\end{equation*}
}

\section{Arbitrage-Free Modeling and State Variable Simulation}

\frame{\frametitle{No Arbitrage Consideration}
The model is free of arbitrage, if:
\begin{itemize}
\item
The discounted zero coupon bond $B(t,T)$ of the OIS rate is a $\Q$-martingale and
\item
The scaled LIBOR rate $\delta\cdot L(t;T,T+\delta)=\left(\frac{\PQ(t,T)}{\PQ(t,T+\delta)}-1\right)$ is a martingale w.r.t. the forward measure $\Q^{T+\delta}$
\end{itemize}
}

\subsection{The OIS Rates}

\frame{\frametitle{1st Drift Condition}
\begin{block}{Zero Coupon Bond Dynamics}
By Ito's Lemma, the dynamics of the zero coupon bond related to the OIS rate are
\begin{equation*}
\frac{dB(t,T)}{B(t,T)}=\left(f(t,t)-A(t,T)+\frac{1}{2}(\Sigma(t,T))^2\right)dt-\Sigma(t,T)dW_f(t)
\end{equation*}
To avoid arbitrage $B(t,T)$ needs to have drift $f(t,t)$ and therefore we must impose
\begin{equation*}
A(t,T)=\frac{1}{2}(\Sigma(t,T))^2
\end{equation*}
\end{block}
}

\frame{\frametitle{Modeling with State Variables(1)}
\begin{block}{State Variable Modeling of Bond $B(t,T)$}
By defining $\sigma_f(t,T):=(\alpha_{0,f}+\alpha_{1,f}(T-t))\exp(-\gamma_f(T-t))$ Trolle and Schwartz derived state variables for $B(t,T)$:
\begin{equation*}
B(t,T)=\frac{B(0,T)}{B(0,t)}\exp\left(\sum_{i=1}^7{C_{f_i}(T-t)\phi_{f_i}(t)}\right)
\end{equation*}
\end{block}
}

\frame{\frametitle{Modeling with State Variables(2)}
The dynamics of the state variables $\phi_{x_i}(t)$ are given by
\begin{equation*}
\begin{split}
d\phi_{f_1}(t)&=-\gamma_f\phi_{f_1}(t)dt+\sqrt{v_f(t)}dW_f(t)\\
d\phi_{f_2}(t)&=(\phi_{f_1}(t)-\gamma_f\phi_{f_2}(t))dt\\
d\phi_{f_3}(t)&=(v_f(t)-\gamma_f\phi_{f_3}(t))dt\\
d\phi_{f_4}(t)&=(v_f(t)-2\gamma_f\phi_{f_4}(t))dt\\
d\phi_{f_5}(t)&=(\phi_{f_3}(t)-\gamma_f\phi_{f_5}(t))dt\\
d\phi_{f_6}(t)&=(\phi_{f_4}(t)-2\gamma_f\phi_{f_6}(t))dt\\
d\phi_{f_7}(t)&=(2\phi_{f_6}(t)-2\gamma_f\phi_{f_7}(t))dt\\
\end{split}
\end{equation*}
}

\frame{\frametitle{Modeling with State Variables(3)}
The variables $C_{y_i}(\tau)$ are given by
\begin{equation*}
\begin{split}
C_{f_1}(\tau)=&\frac{\alpha_{1,f}}{\gamma_f}\bigg(\bigg(\frac{1}{\gamma_f}+\frac{\alpha_{0,f}}{\alpha{1,f}}\bigg)(\exp(-\gamma_f\tau)-1)+\tau\exp(-\gamma_f\tau)\bigg)\\
C_{f_2}(\tau)=&\frac{\alpha_{1,f}}{\gamma_f}(\exp(-\gamma_f\tau)-1)\\
C_{f_3}(\tau)=&\bigg(\frac{\alpha_{1,f}}{\gamma_f}\bigg)^2\bigg(\frac{1}{\gamma_f}+\frac{\alpha_{0,f}}{\alpha{1,f}}\bigg)\bigg(\bigg(\frac{1}{\gamma_f}+\frac{\alpha_{0,f}}{\alpha{1,f}}\bigg)(\exp(-\gamma_f\tau)-1)\\
&+\tau\exp(-\gamma_f\tau)\bigg)\\
C_{f_4}(\tau)=&-\frac{\alpha_{1,f}}{\gamma_f^2}\bigg(\bigg(\frac{\alpha_{1,f}}{2\gamma_f^2}+\frac{\alpha_{0,f}}{\gamma_f}+\frac{\alpha_{0,f}^2}{2\alpha_{1,f}}\bigg)(\exp(-2\gamma_f\tau)-1)\\
&+\bigg(\frac{\alpha_{1,f}}{\gamma_f}+\alpha_{0,f}\bigg)\tau\exp(-2\gamma_f\tau)+\frac{\alpha_{1,f}}{2}\tau^2\exp(-2\gamma_f\tau)\bigg)
\end{split}
\end{equation*}
}

\frame{\frametitle{Modeling with State Variables(4)}
\begin{equation*}
\begin{split}
C_{f_5}(\tau)=&\bigg(\frac{\alpha_{1,f}}{\gamma_f}\bigg)^2\bigg(\frac{1}{\gamma_f}+\frac{\alpha_{0,f}}{\alpha{1,f}}\bigg)(\exp(-\gamma_f\tau)-1)\\
C_{f_6}(\tau)=&-\frac{\alpha_{1,f}}{\gamma_f^2}\bigg(\bigg(\frac{\alpha_{1,f}}{\gamma_f}+\alpha_{0,f}\bigg)(\exp(-2\gamma_f\tau)-1)+\alpha_{1,f}\tau\exp(-2\gamma_f\tau)\bigg)\\
C_{f_7}(\tau)=&-\frac{1}{2}\bigg(\frac{\alpha_{1,f}}{\gamma_f}\bigg)^2(\exp(-2\gamma_f\tau)-1)
\end{split}
\end{equation*}
}

\subsection{The Spread Rate}

\frame{\frametitle{No Arbitrage Considerations}
Absence of arbitrage of LIBOR rate $L(t;T,T+\delta):=\frac{1}{\delta}\left(\frac{\PQ(t,T)}{\PQ(t,T+\delta)}-1\right)$\newline
$\textcolor{deloittegreen}{\Updownarrow}$\newline
$\delta L(t;T,T+\delta)=\left(\frac{\PQ(t,T)}{\PQ(t,T+\delta)}-1\right)$ is a martingale w.r.t. the forward measure $\Q^{T+\delta}$\newline
$\textcolor{deloittegreen}{\Updownarrow}$\newline
$\frac{B(t,T+\delta)\delta L(t,T,T+\delta)}{B_t}$ is a $\Q$-martingale\newline
$\textcolor{deloittegreen}{\Updownarrow}$\newline
The expression $B(t,T+\delta)\frac{\PQ(t,T)}{\PQ(t,T+\delta)}$ has drift $f(t,t)$\newline
$\textcolor{deloittegreen}{\Updownarrow}$\newline
The expression $B(t,T)\frac{\PA(t,T)}{\PA(t,T+\delta)}$ has drift $f(t,t)$
}

\frame{\frametitle{2nd Drift Condition}
\begin{block}{Drift Condition for Spread}
The dynamics of the process $X(t,T,T+\delta):=B(t,T)\frac{\PA(t,T)}{\PA(t,T+\delta)}$ are
\begin{equation*}
\begin{split}
\frac{dX(t;T,T+\delta)}{X(t;T,T+\delta)}=&(f(t,t)-A(t,T))dt-\Sigma(t,T)dW_{f}(s)+(\AD(t,T+\delta)\\
&-\AD(t,T))dt+(\SD(t,T+\delta)-\SD(t,T))dW_{y^\delta}(s)\\
&+\frac{1}{2}(\Sigma(t,T))^2dt+\frac{1}{2}(\SD(t,T+\delta)-\SD(t,T))^2 dt\\
&-\rho_{f,y^\delta}\Sigma(t,T)(\SD(t,T+\delta)-\SD(t,T))dt\\
\end{split}
\end{equation*}
Taking into account the condition on $A(t,T)$ we get the drift condition
\begin{equation*}
\begin{split}
\AD(t,T+\delta)-\AD(t,T)=&-\frac{1}{2}(\SD(t,T+\delta)-\SD(t,T))^2\\
&+\rho_{y^\delta,f}\Sigma(t,T)(\SD(t,T+\delta)-\SD(t,T))
\end{split}
\end{equation*}
\end{block}
}

\frame{\frametitle{Drift Condition on Increments Only}
\begin{block}{Remark: No Unique Drift}
The fictitious bonds $\PQ(t,T)$ are introduced as bonds consistent with the LIBOR rate not only at the level of spot LIBOR rates, so they are defined through $L(t;T,T+\delta)=\frac{1}{\delta}\left(\frac{\PQ(t,T)}{\PQ(t,T+\delta)}-1\right)$. But this does not define the bonds uniquely. For this reason, in contrast to the drift condition for the OIS rate, we only get a condition on $\AD(t,T+\delta)-\AD(t,T)$.
\end{block}
}



\frame{\frametitle{Bond Dynamics}
\begin{block}{Remark: Quotient $\tilde{B}^\delta(t,T)/\tilde{B}^\delta(t,T+\delta)$}
While the drift of $y^\delta(t,T)$ is not defined uniquely as mentioned before, the quotient $\frac{\PA(t,T)}{\PA(t,T+\Delta)}$ is, since:
\begin{equation*}
\begin{split}
\frac{\PA(t,T)}{\PA(t,T+\delta)}=&\frac{\PA(0,T)}{\PA(0,T+\delta)}\exp\bigg(\int_0^t{(\AD(u,T+\delta)-\AD(u,T))du}\\
&+\int_0^t{(\SD(u,T+\delta)-\SD(u,T))dW_{y^\delta}(u)}\bigg)
\end{split}
\end{equation*}
In other words, the drift condition is an explicit drift condition on the quotients.
\end{block}
}

\frame{\frametitle{Grid for Spread Bond Simulation}
\begin{block}{Bond Construction}
In order to construct the explicit bonds, we can utilize the given quotients and formulate the bonds in terms of these quotients. For any $0\le t \le T$ we identify $j\in\mathbb{N}$ such that $0\le T-j\delta-t < \delta$, so we can write
\begin{equation*}
\PA(t,T)=\frac{\PA(t,T)}{\PA(t,T-\delta)}\left(\Pi_{k=1}^{j-1}\frac{\PA(t,T-k\delta)}{\PA(t,T-(k+1)\delta)}\right)\frac{\PA(t,T-j\delta)}{\PA(t,t)}
\end{equation*}
Here, all quotients except $\frac{\PA(t,T-j\delta)}{\PA(t,t)}$ are uniquely defined since they are of the form $\frac{\PA(t,T_i)}{\PA(t,T_i-\delta)}$ for some $T_i\in[t,T]$
\end{block}
}

\frame{\frametitle{Unique Quotient Modeling (1)}
By defining $\sigma_y^\delta(t,T):=\alpha_y^\delta\exp(-\gamma_y^\delta(T-t))$ and recalling $\sigma_f(t,T)=(\alpha_{0,f}+\alpha_{1,f}(T-t))\exp(-\gamma_f(T-t))$ we can derive state variables for the quotients of the form $\frac{\PA(t,T-k\delta)}{\PA(t,T-(k+1)\delta)}$, i.e. we can write:
\begin{equation*}
\begin{split}
\frac{\PA(t,T-k\delta)}{\PA(t,T-(k+1)\delta)}=&\frac{\PA(0,T-k\delta)}{\PA(0,T-(k+1)\delta)}\\
&\exp\left(\sum_{i=1}^5{\hat{C}_{y_i^\delta}(T-(k+1)\delta-t,T-k\delta-t)\phi_{y_i^\delta}(t)}\right)
\end{split}
\end{equation*}
}

\frame{\frametitle{Unique Quotient Modeling (2)}
The state variables $\phi_{y_i^\delta}$ are given by
\begin{equation*}
\begin{split}
d\phi_{y_1^\delta}(t)&=-\gamma_y^\delta\phi_{y_1^\delta}(t)dt+\sqrt{v_y^\delta(t)}dW_{y\delta}(t)\\
d\phi_{y_2^\delta}(t)&=(v_y^\delta(t)-2\gamma_y^\delta\phi_{y_2^\delta}(t))dt\\
d\phi_{y_3^\delta}(t)&=\left(\sqrt{v_y^\delta(t)v_f(t)}-\gamma_y^\delta\phi_{y_3^\delta}(t)\right)dt\\
d\phi_{y_4^\delta}(t)&=\left(\sqrt{v_y^\delta(t)v_f(t)}-(\gamma_y^\delta+\gamma_f)\phi_{y_4^\delta}(t)\right)dt\\
d\phi_{y_5^\delta}(t)&=(\phi_{y_4^\delta}(t)-(\gamma_y^\delta+\gamma_f)\phi_{y_5^\delta}(t))dt
\end{split}
\end{equation*}
}

\frame{\frametitle{Unique Quotient Modeling (3)}
The coefficients $C_{y_i^\delta}(\tau_1,\tau_2)$ are given by
\begin{equation*}
\begin{split}
\hat{C}_{y_1^\delta}(\tau_1,\tau_2)&=\frac{\alpha_y^\delta}{\gamma_y^\delta}(\exp(-\gamma_y^\delta\tau_2)-\exp(-\gamma_y^\delta\tau_1))\\
\hat{C}_{y_2^\delta}(\tau_1,\tau_2)&=\frac{(\alpha_y^\delta)^2}{2(\gamma_y^\delta)^2}(\exp(-\gamma_y^\delta\tau_2)-\exp(-\gamma_y^\delta\tau_1))^2\\
\hat{C}_{y_3^\delta}(\tau_1,\tau_2)&=\rho_{y,f}\frac{\alpha_{1,f}\alpha_y^\delta}{\gamma_f\gamma_y^\delta}\left(\frac{1}{\gamma_f}+\frac{\alpha_{0,f}}{\alpha_{1,f}}\right)(\exp(-\gamma_y^\delta\tau_2)-\exp(-\gamma_y^\delta\tau_1))
\end{split}
\end{equation*}
}

\frame{\frametitle{Unique Quotient Modeling (4)}
\begin{equation*}
\begin{split}
\hat{C}_{y_4^\delta}(\tau_1,\tau_2)=&-\rho_{y,f}\frac{\alpha_{1,f}\alpha_y^\delta}{\gamma_f\gamma_y^\delta}\left(\frac{1}{\gamma_f}+\frac{\alpha_{0,f}}{\alpha_{1,f}}+\tau_1\right)(\exp(-\gamma_f\tau_1)\exp(-\gamma_y^\delta\tau_2)\\
&-\exp(-(\gamma_f+\gamma_y^\delta)\tau_1))\\
\hat{C}_{y_5^\delta}(\tau_1,\tau_2)=&\rho_{y,f}\frac{\alpha_{1,f}\alpha_y^\delta}{\gamma_f\gamma_y^\delta}(\exp(-(\gamma_y^\delta+\gamma_f)\tau_1)-\exp(-\gamma_f\tau_1)\exp(-\gamma_y^\delta\tau_2))
\end{split}
\end{equation*}
}

\frame{\frametitle{How to Deal with the Other Quotients}
\begin{block}{Remark: Simulation of non-unique part}
We do not have many constraints how to approximate the quotient $\frac{\PA(t,T-j\delta)}{\PA(t,t)}$. For continuity, we only need $\frac{\PA(t,T-j\delta)}{\PA(t,t)}\rightarrow 1$ for $T-j\delta-t\searrow 0$ and that the second drift condition holds for $T-j\delta-t\nearrow \delta$. Our approach is therefore to insert the second drift condition for this shorter interval $T-j-t< \delta$, i.e.:
\begin{equation*}
\begin{split}
\frac{\PA(t,T-j\delta)}{\PA(t,t)}=\frac{\PA(0,T-j\delta)}{\PA(0,t)}\exp\left(\sum_{i=1}^5{\hat{C}_{y_i^\delta}(0,T-j\delta-t)\phi_{y_i^\delta}(t)}\right)
\end{split}
\end{equation*}
\end{block}
}

\frame{\frametitle{State Variable Form for Spread Bonds (1)}
\begin{block}{State Variable Form}
Let $\PA(t,T)=\exp\left(-\int_t^T{y^\delta(t,u)du}\right)$ be the spread zero-coupon bond price at time $t$ maturing at time $T$. We can write:
\begin{equation*}
\PA(t,T)=\frac{\PA(0,T)}{\PA(0,t)}\exp\left(\sum_{i=1}^5{C_{y_i^\delta}(T-t)\phi_{y_i^\delta}(t)}\right)
\end{equation*}
with the previously introduced state variables.
\end{block}
}

\frame{\frametitle{State Variable Form for Spread Bonds (2)}
For $\tau_k:=\tau-(j+1-k)\delta$ for $k\in\{1,2,\ldots j+1\}$, we derived the coefficients:
\begin{equation*}
\begin{split}
C_{y_1^\delta}(\tau)=&\frac{\alpha^\delta}{\gamma^\delta}(\exp(-\gamma^\delta\tau)-1)\\
C_{y_2^\delta}(\tau)=&\left(\frac{\alpha^\delta}{\sqrt{2}\gamma^\delta}\right)^2\bigg((\exp(-\gamma^\delta\tau_1)-1)^2+\sum_{k=1}^j{(\exp(-\gamma^\delta\tau_{k+1})}\\
&-\exp(-\gamma^\delta\tau_k))^2\bigg)\\
C_{y_3^\delta}(\tau)=&\rho_{y^\delta,f}\frac{\alpha_{1,f}\alpha^\delta}{\gamma_f\gamma^\delta}\left(\frac{1}{\gamma_f}+\frac{\alpha_{0,f}}{\alpha_{1,f}}\right)(\exp(-\gamma^\delta\tau)-1))
\end{split}
\end{equation*}
}

\frame{\frametitle{State Variable Form for Spread Bonds (3)}
\begin{equation*}
\begin{split}
C_{y_4^\delta}(\tau)=&\rho_{y^\delta,f}\frac{\alpha_{1,f}\alpha^\delta}{\gamma_f\gamma^\delta}\bigg(\left(\frac{1}{\gamma_f}+\frac{\alpha_{0,f}}{\alpha_{1,f}}\right)(1-\exp(-\gamma^\delta\tau_1))+\sum_{k=1}^j{\bigg(\frac{1}{\gamma_f}+\frac{\alpha_{0,f}}{\alpha_{1,f}}}\\
&+\tau_k\bigg)(\exp(-(\gamma^\delta+\gamma_f)\tau_k)-\exp(-\gamma_f\tau_k)\exp(-\gamma^\delta\tau_{k+1}))\bigg)\\
C_{y_5^\delta}(\tau)=&\rho_{y^\delta,f}\frac{\alpha_{1,f}\alpha^\delta}{\gamma_f\gamma^\delta}\bigg((1-\exp(-\gamma^\delta\tau_1))+\sum_{k=1}^j{(\exp(-(\gamma^\delta+\gamma_f)\tau_k)}\\
&-\exp(-\gamma_f\tau_k)\exp(-\gamma^\delta\tau_{k+1}))\bigg)
\end{split}
\end{equation*}
}


\section{Option Pricing}


\frame{\frametitle{Initial Considerations (1)}
A standard approach for option pricing is Fourier transformation. For $X=\log(S_T)$, it will be very useful to calculate $\E[\exp(-\int_t^T{r(s)ds})\exp(uX)]$. In our specific model set up, this transformation can be written as:
\begin{equation*}
\begin{split}
\varphi(u,t,T_0,T_1):=\E_t^\Q\bigg[&\exp\bigg(-\int_t^{T_0}{r(s)ds}+u\log(\PQ(T_0,T_1))\bigg)\bigg]\\
=\E_t\bigg[&\exp\bigg(-\int_t^{T_0}{r(s)ds}+u(\log(\PD(T_0,T_1))\\
&+\log(\PA(T_0,T_1)))\bigg)\bigg]
\end{split}
\end{equation*}
With this transformation, it is very easy to calculate Calls and Puts on $\PQ(t,T)$
}

\frame{\frametitle{Initial Considerations (2)}
We can prove that this transformation can be writen as:
\begin{equation*}
\begin{split}
\varphi(u,t,T_0,T_1)=\exp\bigg(&M(T_0-t)+N(T_0-t)v_f+\tilde{N}(T_0-t)v_y(t)\\
&+\log(B(t,T_0))+u\bigg(\log\bigg(\frac{B(t,T_1)}{B(t,T_0)}\bigg)\\
&+\log\bigg(\frac{\PA(t,T_1)}{\PA(t,T_0)}\bigg)\bigg)\bigg)
\end{split}
\end{equation*}
}

\frame{\frametitle{Initial Considerations (3)}
The triple $(M,N,\tilde{N})$ solves the following system of ODEs
\begin{equation*}
\begin{split}
\frac{dM(\tau)}{d\tau}=&N(\tau)\kappa_f\theta_f+\tilde{N}(\tau)\kappax\thetax+\sqrt{v_f(-\tau+T_0)\vx(-\tau+T_0)}\rho_{\yx,f}\\
&\bigg(u^2(C_{f_1}(\tau_1)-C_{f_1}(\tau))(C_{y_1^\delta}(\tau_1)-C_{y_1^\delta}(\tau))+uC_{f_1}(\tau)(C_{y_1^\delta}(\tau_1)\\
&-C_{y_1^\delta}(\tau))-u\bigg(\sum_{i=0}^{j-1}{C_{f_1}(\tau_1-(i+1)\delta)(C_{y_1^\delta}(\tau_1-i\delta)-C_{y_1^\delta}(\tau_1}\\
&-(i+1)\delta))+C_{f_1}(\tau)(C_{y_1^\delta}(\tau_1-j\delta)-C_{y_1^\delta}(\tau))\bigg)\bigg)\\
\frac{dN(\tau)}{d\tau}=&N(\tau)(-\kappa_f+\sigma_f\rho_{v,f}(C_{f_1}(\tau)+u(C_{f_1}(\tau_1)-C_{f_1}(\tau))))+\frac{1}{2}(N(\tau))^2\sigma_{v_f}^2\\
&+uC_{f_1}(\tau)(C_{f_1}(\tau_1)-C_{f_1}(\tau))+\frac{1}{2}u^2(C_{f_1}(\tau_1)-C_{f_1}(\tau))^2-\frac{1}{2}u\\
&(C_{f_1}(\tau_1)^2-C_{f_1}(\tau)^2)\\
\end{split}
\end{equation*}
}

\frame{\frametitle{Initial Considerations (4)}
\begin{equation*}
\begin{split}
\frac{d\tilde{N}(\tau)}{d\tau}=&\tilde{N}(\tau)(-\kappax+\rho_{v,y^\delta}\sigmax u(C_{y_1^\delta}(\tau_1)-C_{y_1^\delta}(\tau)))+\frac{1}{2}(\tilde{N}(\tau))^2\sigmavx^2+\frac{1}{2}u^2\\
&(C_{y_1^\delta}(\tau_1)-C_{y_1^\delta}(\tau))^2+\frac{1}{2}u\bigg(\sum_{i=0}^{j-1}{(C_{y_1^\delta}(\tau_1-i\delta)-C_{y_1^\delta}(\tau_1-}\\
&(i+1)\delta))^2+(C_{y_1^\delta}(\tau_1-j\delta)-C_{y_1^\delta}(\tau))^2\bigg)
\end{split}
\end{equation*}
Here, $C_{f_1}(\tau)$ and $C_{y_1^\delta}$ are denoting the state variable coefficients and $\tau_1=:T_1-T_0+\tau$. Furthermore, we can approximate the stochastic term in $dM(\tau)$ which is resulting form the non-affinity of the model by a freezing argument:
\begin{equation*}
\sqrt{v_f(t)v_y(t)}\approx\E^\Q[\sqrt{v_f(t)v_y(t)}]=\E^\Q[\sqrt{v_f(t)}]\E^\Q[\sqrt{v_y(t)}]
\end{equation*}
}

\frame{\frametitle{Put and Call Option}
\begin{block}{Price of Puts and Calls on Zero Coupon Bond}
With the previously introduced transformation, the price of an option on $\overline{P}(T_0,T_1)$ at time $t$ with strike $K$ is given by
\begin{equation*}
\begin{split}
O(t,T_0,T_1,K,\omega)&=\E_t^\Q[\exp(-\int_t^{T_0}{r(s)ds})(\omega\overline{P}(T_0,T_1)-\omega K)^+]\\
&=\omega G_{1,-\omega}(-\omega\log(K))-\omega K G_{0,-\omega}(-\omega\log(K))
\end{split}
\end{equation*}
with $\omega=1$ for Call and $\omega=-1$ for Put options and where $G$ is given by
\begin{equation*}
\begin{split}
G_{a,b}(y)&=\E^\Q_t[\exp(-\int_t^{T_0}(r(s)ds))\exp(a\log(\overline{P}(T_0,T_1)))\boldsymbol{1}_{\{b\log(\overline{P}(T_0,T_1))<y\}}]\\
&=\frac{\varphi(a,t,T_0,T_1)}{2}-\frac{1}{\pi}\int_0^\infty{\frac{\mbox{Im}[\varphi(a+iub,t,T_0,T_1)\exp(-iuy)]}{u}du}
\end{split}
\end{equation*}
\end{block}
}

\section{Verification}

\frame{\frametitle{Output: Calculation (1)}
The code determined the following Monte Carlo prices based on 10000 simulations. For the put option on the zero coupon bond, Deloitte uses the strike $K=0.95$. For the put price, Deloitte also developed a closed formula:
\begin{center}
\includegraphics{"output matlab".PNG}
\end{center}
}

\frame{\frametitle{Output: Calculation (2)}
\begin{itemize}
\item[$\Rightarrow$]
The prices for the put option have a small gap
\item[$\Rightarrow$]
When we take a look at the the put prices w.r.t. the strike price, we see that beside a small Monte Carlo error, the closed formula has a classical numerical miscalculation resulting from the fourier inversion method
\end{itemize}
}

\frame{\frametitle{Plot: Put Prices w.r.t. Strike}
For small strikes, we see a highly oscillating closed formula price:
\begin{center}
\includegraphics{"plot normal".PNG}
\end{center}
}

\frame{\frametitle{Plot: Put Prices w.r.t. Strike (Zoom)}
The closed formula is also oscillating slightly above Monte Carlo:
\begin{center}
\includegraphics{"plot zoom".PNG}
\end{center}
}

\frame{\frametitle{Conclusion: The Gap is Caused By Numerical Issues}
\begin{itemize}
\item
The gap between closed formula and Monte Carlo price can be explained by typical numerical errors of the Fourier inversion method
\item
We therefore consider the Monte Carlo simulation as well working
\end{itemize}
}

%\frame{\frametitle{Proof}
%\begin{equation*}
%\begin{split}
%\visible<1->{\E[&\exp(\langle u,\tilde{X}_t\rangle)|\sa_s]=\\}
%\visible<2->{& =\E[\E[\exp(\langle u, X_1^{\tilde{X}_{t-1}}+\Delta Y_{t-1}\rangle)|\sa_{t-1}]|\sa_s] \\}
%\visible<3->{& =\E[\E[\exp(\langle u,\Delta Y_{t-1}\rangle)]\E[\exp(\langle u,X_1^{\tilde{X}_{t-1}}\rangle|\sa_{t-1}]|\sa_s] \\}
%\visible<4->{& =\E[\exp(\mu(u,t-1))\exp(\phi(u,1)+\langle\psi(u,1),\tilde{X}_{t-1}\rangle|\sa_s] \\}
%\visible<5->{& =\E[\E[\exp(\phi(u,1)+\mu(u,t-1))\exp(\langle\psi(u,1),\tilde{X}_{t-1}\rangle|\sa_{t-2}]|\sa_s] \\}
%\visible<6->{& =\E[\exp(\phi(u,1)+\mu(u,t-1))\E[\exp(\langle\psi(u,1),\tilde{X}_{t-1}\rangle|\sa_{t-2}]|\sa_s] \\}
%\visible<7->{& =\E[\exp(\phi(u,1)+\mu(u,t-1))\exp(\phi(\psi(u,1),1)+\mu(\psi(u,1),t-2) \\
%& \hspace{1cm}+\langle\psi(\psi(u,1),1),\tilde{X}_{t-2}\rangle)|\sa_s] \\}
%\visible<8->{& =\E[\exp(\mu(u,t-1)+\mu(\psi(u,1),t-2)+\phi(u,2)+\langle\psi(u,2),\tilde{X}_{t-2}\rangle)|\sa_s]\\}
%\visible<9->{& =\ldots=\exp(\phi(u,t-s)+\langle\psi(u,t-s),\tilde{X}_s\rangle+\sum\limits_{k=1}^{t-s}{\mu(\psi(u,k-1),t-k)})}
%\end{split}
%\end{equation*}
%}

\frame{\frametitle{Thank you}
\begin{center}
\Large{
Thank you for your kind attention}
\end{center}
}
\end{document}

